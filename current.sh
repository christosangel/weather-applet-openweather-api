#! /bin/bash
if [[ ! -d /tmp/weather/ ]]
then
	mkdir -p /tmp/weather/box/
fi
#Get rid of previous data
unset B[@]
rm  /tmp/weather/box/*.txt /tmp/weather/days.txt
API_1=$(grep "forecast?" ~/openweather.txt)
API_2=$(grep "weather?" ~/openweather.txt)
curl $API_1  -o /tmp/weather/weather.json
curl $API_2  -o /tmp/weather/current.json

i=0
while [ $i -le 38 ]
do
	ID[$i]=$(jq '.list['$i'].dt' /tmp/weather/weather.json)
	DT[$i]=$(date --date="@$(jq '.list['$i'].dt' /tmp/weather/weather.json)")
	#echo "ID "${ID[$i]}
	#echo "Day "$(date --date="@$(jq '.list['$i'].dt' /tmp/weather/weather.json)")
	echo ${ID[$i]}>/tmp/weather/box/${ID[$i]}.txt
	echo ${DT[$i]}>>/tmp/weather/box/${ID[$i]}.txt
	#DTTEXT[$i]=$(jq '.list['$i'].dt_txt' /tmp/weather/weather.json)
	#echo "DT Text " ${DTTEXT[$i]}
	#echo ${DTTEXT[$i]}>>/tmp/weather/box/${ID[$i]}.txt
	#MAIN[$i]="$(jq '.list['$i'].weather[0].main' /tmp/weather/weather.json)"
	ICON[$i]=$(jq '.list['$i'].weather[0].icon' /tmp/weather/weather.json)
	TEMPERATURE[$i]="<span foreground='yellow'>"$(( $(jq '.list['$i'].main.temp' /tmp/weather/weather.json|sed 's/\..*$//') - 273 ))"°C</span>"
	#echo "Condition " ${MAIN[$i]} ${ICON[$i]}
	echo ${ICON[$i]} ${TEMPERATURE[$i]}>>/tmp/weather/box/${ID[$i]}.txt
	DESCRIPTION[$i]=$(jq '.list['$i'].weather[0].description' /tmp/weather/weather.json)
	#echo "Description " ${DESCRIPTION[$i]}
	echo ${DESCRIPTION[$i]}>>/tmp/weather/box/${ID[$i]}.txt
	#echo "Icon " ${ICON[$i]}
	#echo "Temperature "${TEMPERATURE[$i]}
	#echo ${TEMPERATURE[$i]}>>/tmp/weather/box/${ID[$i]}.txt
	HUMIDITY[$i]=$(jq '.list['$i'].main.humidity' /tmp/weather/weather.json)"%"
	#echo "Humidity "${HUMIDITY[$i]}
	echo ${HUMIDITY[$i]}>>/tmp/weather/box/${ID[$i]}.txt


	WIND[$i]=$(jq '.list['$i'].wind.speed' /tmp/weather/weather.json)" m/sec"

	DEGREE[$i]=$(jq '.list['$i'].wind.deg' /tmp/weather/weather.json)
	case ${DEGREE[$i]} in
  2[3-9|3[0-9]|4[0-9]|5[0-9]|6[0-8])
  DIRECTION[$i]="↙";;
  6[7-9]|7[0-9]|8[0-9]|9[0-9]|10[0-9]|11[0-2])
  DIRECTION[$i]="←";;
  11[3-9]|12[0-9]|13[0-9]|14[0-9]|15[0-7])
  DIRECTION[$i]="↖";;
  15[8-9]|16[0-9]|17[0-9]|18[0-9]|19[0-9]|20[0-2])
  DIRECTION[$i]="↑";;
  20[3-9]|21[0-9]|22[0-9]|23[0-9]|24[0-7])
  DIRECTION[$i]="↗";;
  24[8-9]|25[0-9]|26[0-9]|27[0-9]|28[0-9]|29[0-2])
  DIRECTION[$i]="→";;
  29[3-9]|30[0-9]|31[0-9]|32[0-9]|33[0-7])
  DIRECTION[$i]="↘";;
  33[8-0]|34[0-9]|35[0-9]|360|[0-9]|1[0-9]|2[0-3])
  DIRECTION[$i]="↓";;
esac
	echo ${WIND[$i]}" " ${DIRECTION[$i]} >>/tmp/weather/box/${ID[$i]}.txt
	#echo "Wind " ${WIND[$i]} ${DIRECTION[$i]}
	#echo "Degrees " ${DEGREE[$i]}
	#echo ${DEGREE[$i]}>>/tmp/weather/box/${ID[$i]}.txt
	#echo "=================="
	((i++))
done
#CITY=$(jq '.city.name'  /tmp/weather/weather.json)","$(jq '.city.country'  /tmp/weather/weather.json)
#echo "$CITY"|sed 's/\"//g'
#COORD1=$(jq '.city.coord.lat'  /tmp/weather/weather.json)
#COORD2=$(jq '.city.coord.lon'  /tmp/weather/weather.json)
#echo "Coordinates " $COORD1 " + "$COORD2
#SUNRISE=$(date --date="@$(jq '.city.sunrise'  /tmp/weather/weather.json)"|awk '{print $5" "$6}')
#echo "Sunrise " $SUNRISE
#SUNSET=$(date --date="@$(jq '.city.sunset'  /tmp/weather/weather.json)"|awk '{print $5 " "$6}')
#echo "Sunset " $SUNSET


##void
#if [[ $(cat /tmp/weather/box/$(ls /tmp/weather/box/|head -1)) = *"03:00:00 AM"* ]]; then touch /tmp/weather/box/1.txt ;fi
#if [[ $(cat /tmp/weather/box/$(ls /tmp/weather/box/|head -1)) = *"06:00:00 AM"* ]]; then touch /tmp/weather/box/{1..2}.txt ;fi
#if [[ $(cat /tmp/weather/box/$(ls /tmp/weather/box/|head -1)) = *"09:00:00 AM"* ]]; then touch /tmp/weather/box/{1..3}.txt ;fi
#if [[ $(cat /tmp/weather/box/$(ls /tmp/weather/box/|head -1)) = *"12:00:00 PM"* ]]; then touch /tmp/weather/box/{1..4}.txt ;fi
#if [[ $(cat /tmp/weather/box/$(ls /tmp/weather/box/|head -1)) = *"03:00:00 PM"* ]]; then touch /tmp/weather/box/{1..5}.txt ;fi
#if [[ $(cat /tmp/weather/box/$(ls /tmp/weather/box/|head -1)) = *"06:00:00 PM"* ]]; then touch /tmp/weather/box/{1..6}.txt ;fi
#if [[ $(cat /tmp/weather/box/$(ls /tmp/weather/box/|head -1)) = *"09:00:00 PM"* ]]; then touch /tmp/weather/box/{1..7}.txt ;fi


##NEW void
for i in $(ls /tmp/weather/box|sort -h)
do
 head -2 /tmp/weather/box/$i|tail +2|awk '{print $1,$2,$3}' >>/tmp/weather/days.txt
done
TODAY=$(head -1 /tmp/weather/days.txt )
#echo "today="$TODAY
TODAYBOXES=$(grep "$TODAY" /tmp/weather/days.txt|wc -l)
#echo "todayboxes="$TODAYBOXES
VOIDBOXES=$(( 8 - $TODAYBOXES ))
#echo "VOIDBOXES="$VOIDBOXES
if [ $VOIDBOXES != 0 ]
then
	v=1
	while [ $v -le $VOIDBOXES ]
	do
 	touch /tmp/weather/box/$v.txt
 	((v++))
 done
fi

sed -i '$!N; /^\(.*\)\n\1$/!P; D' /tmp/weather/days.txt
##days

echo "<b>$(head -1 /tmp/weather/days.txt|tail +1)</b>">/tmp/weather/box/0.txt
echo "<b>$(head -2 /tmp/weather/days.txt|tail +2)</b>">/tmp/weather/box/$(( $(head -1 /tmp/weather/box/$(ls /tmp/weather/box|sort -h|head -9|tail +9)) + 1 )).txt
echo "<b>$(head -3 /tmp/weather/days.txt|tail +3)</b>">/tmp/weather/box/$(( $(head -1 /tmp/weather/box/$(ls /tmp/weather/box|sort -h|head -18|tail +18)) +1 )).txt
echo "<b>$(head -4 /tmp/weather/days.txt|tail +4)</b>">/tmp/weather/box/$(( $(head -1 /tmp/weather/box/$(ls /tmp/weather/box|sort -h|head -27|tail +27)) +1 )).txt
echo "<b>$(head -5 /tmp/weather/days.txt|tail +5)</b>">/tmp/weather/box/$(( $(head -1 /tmp/weather/box/$(ls /tmp/weather/box|sort -h|head -36|tail +36)) +1 )).txt
if [[ -n $(head -6 /tmp/weather/days.txt|tail +6) ]] || [[ $(ls /tmp/weather/box/*.txt|wc -l) -gt 45 ]]
then
 echo "<b>$(head -6 /tmp/weather/days.txt|tail +6)</b>">/tmp/weather/box/$(( $(head -1 /tmp/weather/box/$(ls /tmp/weather/box|sort -h|head -45|tail +45)) +1 )).txt
fi
###PARSE ICONS
sed -i 's/"01d"/☀️/g;s/"01n"/🌙/g;s/"02d"/⛅️/g;s/"02n"/☁️🌙/g;s/"03d"/⛅️/g;s/"03n"/☁️🌙/g;s/"04d"/☁️/g;s/"04n"/☁️/g;s/"09d"/🌧️/g;s/"09n"/🌧️/g;s/"10d"/🌧️/g;s/"10n"/🌧️/g;s/"11d"/⛈️/g;s/"11n"/⛈️/g;s/"13d"/❄️/g;s/"13n"/❄️/g;s/"50d"/🌫/g;s/"50d"/🌫/g;s/"//g' /tmp/weather/box/*.txt

for f in $(ls /tmp/weather/box/|sort -h)
do
 ff=${f%.txt}
 B[$ff]="$(tail -4 /tmp/weather/box/$f)"
done
######################################################

dt=$(date --date="@$(jq '.dt' /tmp/weather/current.json)"|awk '{print $5,$6}')
#echo "Updated " $dt
CITY=$(echo $(jq '.name'  /tmp/weather/current.json)","$(jq '.sys.country'  /tmp/weather/current.json)|sed 's/\"//g')
#echo "$CITY"
COORD1=$(jq '.coord.lat'  /tmp/weather/current.json)
COORD2=$(jq '.coord.lon'  /tmp/weather/current.json)
#echo "Coordinates " $COORD1 " + "$COORD2
SUNRISE=$(date --date="@$(jq '.sys.sunrise'  /tmp/weather/current.json)"|awk '{print $5" "$6}')
#echo "Sunrise :" $SUNRISE
SUNSET=$(date --date="@$(jq '.sys.sunset'  /tmp/weather/current.json)"|awk '{print $5 " "$6}')
#echo "Sunset :" $SUNSET
CURRENT_ICON=$(echo $(jq '.weather[0].icon' /tmp/weather/current.json)|sed 's/\"//g')
#echo "CURRENT_ICON=" $CURRENT_ICON
CONDITION=$(echo $(jq '.weather[0].description' /tmp/weather/current.json)|sed 's/\"//g')
#echo "Condition :" $CONDITION
#echo "Icon :" $ICON
TEMPERATURE=$(( $(jq '.main.temp' /tmp/weather/current.json|sed 's/\..*$//') - 273 ))"°C"
#echo "Temperature :"$TEMPERATURE
HUMIDITY=$(jq '.main.humidity' /tmp/weather/current.json)"%"
#echo "Humidity :"$HUMIDITY
WIND=$(jq '.wind.speed' /tmp/weather/current.json)" m/sec"
#echo "Wind " ${WIND[$i]}
MOIRES=$(jq '.wind.deg' /tmp/weather/current.json)
case $MOIRES in
  2[3-9|3[0-9]|4[0-9]|5[0-9]|6[0-8])
  KATEUTHYNSH="↙";;
  6[7-9]|7[0-9]|8[0-9]|9[0-9]|10[0-9]|11[0-2])
  KATEUTHYNSH="←";;
  11[3-9]|12[0-9]|13[0-9]|14[0-9]|15[0-7])
  KATEUTHYNSH="↖";;
  15[8-9]|16[0-9]|17[0-9]|18[0-9]|19[0-9]|20[0-2])
  KATEUTHYNSH="↑";;
  20[3-9]|21[0-9]|22[0-9]|23[0-9]|24[0-7])
  KATEUTHYNSH="↗";;
  24[8-9]|25[0-9]|26[0-9]|27[0-9]|28[0-9]|29[0-2])
  KATEUTHYNSH="→";;
  29[3-9]|30[0-9]|31[0-9]|32[0-9]|33[0-7])
  KATEUTHYNSH="↘";;
  33[8-0]|34[0-9]|35[0-9]|360|[0-9]|1[0-9]|2[0-3])
  KATEUTHYNSH="↓";;
esac



#echo "Wind :"$WIND" " $MOIRES
declare -A PAIR_IMAGE
ICON=("01d"\
						"01n"\
						"02d"\
						"02n"\
						"03d"\
						"03n"\
						"04d"\
						"04n"\
						"09d"\
						"09n"\
						"10d"\
						"10n"\
						"11d"\
						"11n"\
						"13d"\
						"13n"\
						"50d"\
						"50n")
IMAGE=("weather-clear" \
 "weather-clear-night" \
 "weather-few-clouds" \
 "weather-few-clouds-night" \
 "weather-overcast" \
 "weather-overcast" \
 "weather-overcast" \
 "weather-overcast" \
 "weather-showers" \
 "weather-showers" \
 "weather-showers" \
 "weather-showers" \
 "weather-storm" \
 "weather-storm" \
 "weather-snow" \
 "weather-snow" \
 "weather-fog" \
 "weather-fog")
s=0
sMAX=$(( ${#ICON[@]} - 1 ))
while [ $s -le $sMAX ]
do
 IMG=${IMAGE[$s]}
 #echo "IMG=" $IMG
 IC=${ICON[$s]}
	#echo "IC="$IC
	PAIR_IMAGE["$IC"]="$IMG"


	#echo "PAIR_IMAGE[""$IC""]="${PAIR_IMAGE[$IC]}
	#echo "PAIR_CHAR[""$STAT""]="${PAIR_CHAR[$STAT]}
	((s++))
done
CURRENT_IMAGE=${PAIR_IMAGE[$CURRENT_ICON]}
#echo "CURRENT IMAGE :"$CURRENT_IMAGE
######################################################
##REname txt to text in order to make yad inline
#i=1;imax=$(ls /tmp/weather/box/*.txt|wc -l);while [ $i -le $imax ];do cp $(ls /tmp/weather/box/*.txt|sort -h|head -$i|tail +$i)  /tmp/weather/box/$i.text;((i++));done;rm /tmp/weather/box/*.txt

###############################################




TOOLTIP=" Temperature :$TEMPERATURE\n
Conditions :$CONDITION \n
Humidity :$HUMIDITY\n
Wind :$WIND $KATEUTHYNSH\n
Updated :$dt\n
Location :$CITY\n
Coordinates :$COORD1 : $COORD2\n
Sunrise :$SUNRISE\n
Sunset :$SUNSET\n"
#echo -e "$TOOLTIP"
if [ $DESKTOP_SESSION = "cinnamon" ]
then
	echo -e "<xml>
	<appsettings>
	<tooltip>
"$TOOLTIP"
	</tooltip>
	<clickaction>/home/christos/git/weather-applet-openweather-api/yad.sh</clickaction>

	</appsettings>

	<item>
	<type>icon</type>
		<value>/usr/share/icons/gnome/24x24/status/$CURRENT_IMAGE.png</value>
		<attr>
			<style>icon-size: 2.4em;</style>
		</attr>
	</item>
	<item>
		<type>text</type>
		<attr>
			<style>font-size: 10pt;font-weight: bold;color: silver</style>
		</attr>
		<value>"$TEMPERATURE"</value>
		</item>

</xml>"
elif [ $DESKTOP_SESSION = "xfce" ]
then
		echo -e "	<tool>"$TOOLTIP"	</tool>
		<click>/home/christos/git/weather-applet-openweather-api/yad.sh</click>
		<img>/usr/share/icons/gnome/24x24/status/$CURRENT_IMAGE.png</img>
		<txt><b>"$TEMPERATURE""   "</b></txt>"
fi
