# weather-applet-openweather-api

A weather applet for xfce or cinnamon Desktop environment, using the free Openweather API

https://openweathermap.org/

## INSTALL

 * At home directory create the folder git/weather-applet-openweather-api:

>$ cd

>$ mkdir -p git/weather-applet-openweather-api

and add there the _current.sh_ and _yad.sh_ files.

 * Visit [https://openweathermap.org/](https://openweathermap.org/)
 and follow the instructions to get forcast and current weather free APIs.

 * Write those two APIs in _openweather.txt_ file, placed in your home directory, so that they can be retrieved by _current.sh_.

 * Make _current.sh_ and _yad.sh_ files executable:

> chmod +x ~/git/weather-applet-openweather-api/{current.sh,yad.sh}

### ADD THE APPLET TO THE PANEL

**FOR XFCE Desktop Environment:**

Use General Monitor Applet


**FOR CINNAMON Desktop Environment:**

 1. Add CommandRunner Applet to the panel
 2. In the Command field, add path to _current.sh_
 3. Recommended Run interval: 9000 secs




